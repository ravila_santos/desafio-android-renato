package br.com.desafioconcrete;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import br.com.desafioconcrete.fragment.FragmentPullRequest;
import br.com.desafioconcrete.util.Util;

public class PullRequestActivity extends AppCompatActivity {

    private static final String FRAGMENT_PR = "FRAGMENT_PR";
    private FragmentPullRequest fragment;
    private TextView tvPROpened;
    private TextView tvPRClosed;

    private String ownerName;
    private String repoName;

    public String getOwnerName() {
        return ownerName;
    }

    public String getRepoName() {
        return repoName;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(FRAGMENT_PR, fragment);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if(getIntent() != null) {
            ownerName = getIntent().getStringExtra(Util.PREF_REPO_OWNER);
            repoName = getIntent().getStringExtra(Util.PREF_REPO_NAME);
            setTitle(repoName);
        }

        if(savedInstanceState == null) {
            fragment = new FragmentPullRequest();
        }
        else {
            fragment = (FragmentPullRequest) savedInstanceState.getSerializable(FRAGMENT_PR);
        }

        tvPROpened = (TextView) findViewById(R.id.tvPrOpened);
        tvPRClosed = (TextView) findViewById(R.id.tvPrClosed);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentContainer, fragment, "frag_pr").commit();
    }

    public void setPrOpen(int total) {
        tvPROpened.setText(getString(R.string.count_pr_opened, ""+total));
    }

    public void setPrClose(int total) {
        tvPRClosed.setText(getString(R.string.count_pr_closed, ""+total));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
