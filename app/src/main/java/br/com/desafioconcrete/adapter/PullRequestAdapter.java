package br.com.desafioconcrete.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.text.SimpleDateFormat;
import java.util.List;

import br.com.desafioconcrete.R;
import br.com.desafioconcrete.model.PullRequest;

/**
 * Criado por Renato em 29/11/2016.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private LayoutInflater mInflater;
    private List<PullRequest> pullRequests;
    private Context mContext;

    public PullRequestAdapter(Context context, List<PullRequest> pullRequests) {
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
        this.pullRequests = pullRequests;
    }

    public void setPullRequests(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(pullRequests != null)
            return pullRequests.size();
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(mInflater.inflate(R.layout.item_pull_request, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof ItemViewHolder) {
            final ItemViewHolder holder = (ItemViewHolder) viewHolder;
            final PullRequest pr = pullRequests.get(position);

            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

            holder.tvPRName.setText(pr.getTitle());
            holder.tvPRDesc.setText(pr.getBody());
            holder.tvPRDate.setText(format.format(pr.getCreated_at()));
            holder.tvPRUsername.setText(pr.getUser().getLogin());
            Uri uri = Uri.parse(pr.getUser().getAvatar_url());
            holder.ivPRUserLogo.setImageURI(uri);
        }
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder {
        public final RelativeLayout llAllLayout;
        public final TextView tvPRName;
        public final TextView tvPRDesc;
        public final TextView tvPRDate;
        public final TextView tvPRUsername;
        public final SimpleDraweeView ivPRUserLogo;

        public ItemViewHolder(View view) {
            super(view);
            this.llAllLayout = (RelativeLayout) view.findViewById(R.id.llAllLayout);
            this.tvPRName = (TextView) view.findViewById(R.id.tvPRName);
            this.tvPRDesc = (TextView) view.findViewById(R.id.tvPRDesc);
            this.tvPRDate = (TextView) view.findViewById(R.id.tvPRDate);
            this.tvPRUsername = (TextView) view.findViewById(R.id.tvPRUsername);
            this.ivPRUserLogo = (SimpleDraweeView) view.findViewById(R.id.ivPRUserLogo);
        }
    }
}
