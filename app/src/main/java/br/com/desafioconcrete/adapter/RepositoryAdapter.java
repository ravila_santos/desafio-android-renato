package br.com.desafioconcrete.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import br.com.desafioconcrete.PullRequestActivity;
import br.com.desafioconcrete.R;
import br.com.desafioconcrete.fragment.FragmentRepository;
import br.com.desafioconcrete.model.Repository;
import br.com.desafioconcrete.util.Util;

/**
 * Criado por Renato em 29/11/2016.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_PROGRESS = 1;

    private LayoutInflater mInflater;
    private List<Repository> repositories;
    private Context mContext;
    private FragmentRepository frag;

    public RepositoryAdapter(Context context, FragmentRepository frag, List<Repository> repositories) {
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
        this.repositories = repositories;
        this.frag = frag;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(repositories != null) {
            if(frag.isReachEnd())
                return repositories.size();
            return repositories.size() + 1;
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == getItemCount()-1 && !frag.isReachEnd())
            return TYPE_PROGRESS;

        return TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_ITEM)
            return new ItemViewHolder(mInflater.inflate(R.layout.item_repository, parent, false));

        return new ProgressItemViewHolder(mInflater.inflate(R.layout.item_progress_repository, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if ((position == getItemCount() - 3))
            frag.loadMoreData();

        if (viewHolder instanceof ItemViewHolder) {
            final ItemViewHolder holder = (ItemViewHolder) viewHolder;
            final Repository rep = repositories.get(position);

            holder.tvRepoName.setText(rep.getName());
            holder.tvRepoDesc.setText(rep.getDescription());
            holder.tvRepoFork.setText(""+rep.getForks_count());
            holder.tvRepoStar.setText(""+rep.getStargazers_count());
            holder.tvRepoUsername.setText(rep.getOwner().getLogin());
            Uri uri = Uri.parse(rep.getOwner().getAvatar_url());
            holder.ivRepoUserLogo.setImageURI(uri);

            holder.llAllLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, PullRequestActivity.class);
                    i.putExtra(Util.PREF_REPO_OWNER, rep.getOwner().getLogin());
                    i.putExtra(Util.PREF_REPO_NAME, rep.getName());
                    mContext.startActivity(i);
                }
            });
        }
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder {
        public final RelativeLayout llAllLayout;
        public final TextView tvRepoName;
        public final TextView tvRepoDesc;
        public final TextView tvRepoFork;
        public final TextView tvRepoStar;
        public final TextView tvRepoUsername;
        public final SimpleDraweeView ivRepoUserLogo;

        public ItemViewHolder(View view) {
            super(view);
            this.llAllLayout = (RelativeLayout) view.findViewById(R.id.llAllLayout);
            this.tvRepoName = (TextView) view.findViewById(R.id.tvRepoName);
            this.tvRepoDesc = (TextView) view.findViewById(R.id.tvRepoDesc);
            this.tvRepoFork = (TextView) view.findViewById(R.id.tvRepoFork);
            this.tvRepoStar = (TextView) view.findViewById(R.id.tvRepoStar);
            this.tvRepoUsername = (TextView) view.findViewById(R.id.tvRepoUsername);
            this.ivRepoUserLogo = (SimpleDraweeView) view.findViewById(R.id.ivRepoUserLogo);
        }
    }

    private static class ProgressItemViewHolder extends RecyclerView.ViewHolder {
        public final ProgressBar progress;

        public ProgressItemViewHolder(View view) {
            super(view);
            this.progress = (ProgressBar) view.findViewById(R.id.progress);
        }
    }
}
