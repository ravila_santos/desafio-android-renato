package br.com.desafioconcrete.model;

import java.io.Serializable;

/**
 * Criado por Renato em 29/11/2016.
 */

public class Repository implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String description;
    private Owner owner;
    private Long forks_count;
    private Long stargazers_count;

    public Repository() {

    }

    public Repository(Long id, String name, String description, Owner owner, Long forks_count, Long stargazers_count) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.owner = owner;
        this.forks_count = forks_count;
        this.stargazers_count = stargazers_count;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Long getForks_count() {
        return forks_count;
    }

    public void setForks_count(Long forks_count) {
        this.forks_count = forks_count;
    }

    public Long getStargazers_count() {
        return stargazers_count;
    }

    public void setStargazers_count(Long stargazers_count) {
        this.stargazers_count = stargazers_count;
    }
}
