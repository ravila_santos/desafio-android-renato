package br.com.desafioconcrete.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Criado por Renato em 29/11/2016.
 */

public class PullRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String title;
    private String body;
    private String state;
    private Date created_at;
    private UserPR user;

    public PullRequest() {
    }

    public PullRequest(Long id, String title, String body, String state, Date created_at, UserPR user) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.state = state;
        this.created_at = created_at;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public UserPR getUser() {
        return user;
    }

    public void setUser(UserPR user) {
        this.user = user;
    }
}
