package br.com.desafioconcrete.model;

import java.io.Serializable;

/**
 * Criado por Renato em 29/11/2016.
 */

public class Owner implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String login;
    private String avatar_url;

    public Owner() {
    }

    public Owner(Long id, String login, String avatar_url) {
        this.id = id;
        this.login = login;
        this.avatar_url = avatar_url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }
}
