package br.com.desafioconcrete.model;

import java.io.Serializable;
import java.util.List;

/**
 * Criado por Renato em 29/11/2016.
 */

public class Repositories implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<Repository> items;

    public Repositories() {
    }

    public Repositories(List<Repository> items) {
        this.items = items;
    }

    public List<Repository> getItems() {
        return items;
    }

    public void setItems(List<Repository> items) {
        this.items = items;
    }
}
