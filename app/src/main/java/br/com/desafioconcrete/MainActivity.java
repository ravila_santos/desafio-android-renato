package br.com.desafioconcrete;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import br.com.desafioconcrete.fragment.FragmentRepository;

public class MainActivity extends AppCompatActivity {

    private static final String FRAGMENT_REPO = "FRAGMENT_REPO";
    private FragmentRepository fragment;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(FRAGMENT_REPO, fragment);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        if(savedInstanceState == null) {
            fragment = new FragmentRepository();
        }
        else {
            fragment = (FragmentRepository) savedInstanceState.getSerializable(FRAGMENT_REPO);
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentContainer, fragment, "home").commit();
    }
}
