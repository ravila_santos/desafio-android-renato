package br.com.desafioconcrete.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import br.com.desafioconcrete.model.PullRequest;
import br.com.desafioconcrete.model.Repositories;

/**
 * Criado por Renato em 29/11/2016.
 */

public class GsonParser {

    private Gson gson;

    public GsonParser() {
        gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();
    }

    public Repositories toRepositories(String jsonString) {
        Type type = new TypeToken<Repositories>(){}.getType();
        return gson.fromJson(jsonString, type);
    }

    public List<PullRequest> toPullRequestList(String jsonString) {
        Type type = new TypeToken<List<PullRequest>>(){}.getType();
        return gson.fromJson(jsonString, type);
    }
}
