package br.com.desafioconcrete.util;

import br.com.desafioconcrete.R;

/**
 * Criado por Renato em 29/11/2016.
 */

public class Util {

    public static final int RESPONSE_OK = 1;
    public static final int RESPONSE_ERROR = -1;
    public static final String PREF_REPO_OWNER = "PREF_REPO_OWNER";
    public static final String PREF_REPO_NAME = "PREF_REPO_NAME";
    public static final String SHARED_PREFERENCES = "preferences_desafio";

    public static int[] getColorsSwipe() {
        int[] cors = new int[3];
        cors[0] = R.color.colorPrimary;
        cors[1] = R.color.colorAccent;
        cors[2] = R.color.title_color;

        return cors;
    }
}
