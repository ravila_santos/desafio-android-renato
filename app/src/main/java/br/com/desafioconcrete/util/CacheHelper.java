package br.com.desafioconcrete.util;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Criado por Renato em 29/11/2016.
 */

public class CacheHelper {

    private Context mContext;

    public CacheHelper(Context mContext) {
        this.mContext = mContext;
    }

    public void saveJson(String key, Long id, String json) {
        saveJson(key + id, json);
    }

    public String getSavedJson(String key, Long id) {
        return getSavedJson(key + id);
    }

    private void saveJson(String path, String value) {
        try {
            File cache = new File(mContext.getCacheDir() + "/" + path + ".srl");

            FileOutputStream out = new FileOutputStream(cache);
            out.write(value.getBytes());
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getSavedJson(String path) {
        try {
            File cache = new File(mContext.getCacheDir() + "/" + path + ".srl");

            if (cache.exists()) {
                FileInputStream in = new FileInputStream(cache);
                int size = in.available();
                byte[] buffer = new byte[size];
                in.read(buffer);
                in.close();

                return new String(buffer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
