package br.com.desafioconcrete.volley;


import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import br.com.desafioconcrete.application.AppController;

/**
 * Criado por Renato em 29/11/2016.
 */

public class RequestHelper {

    public static void getDataString(String url, Listener<String> success, ErrorListener error) {
        makeRequest(Method.GET, url, success, error);
    }

    public static void getDataJson(String url, Listener<JSONObject> success, ErrorListener error) {
        makeRequestJson(Method.GET, url, success, error);
    }

    private static void makeRequest(int method, String url, Listener<String> success, ErrorListener error) {
        Log.d("JSON_OBJECT", "connecting to: " + url);

        StringRequest request = new StringRequest(method, url, success, error);
        request.setRetryPolicy(new DefaultRetryPolicy(30000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);
    }

    private static void makeRequestJson(int method, String url, Listener<JSONObject> success, ErrorListener error) {
        Log.d("JSON_OBJECT", "connecting to: " + url);

        JsonObjectRequest request = new JsonObjectRequest(method, url, null, success, error);
        request.setRetryPolicy(new DefaultRetryPolicy(30000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);
    }
}
