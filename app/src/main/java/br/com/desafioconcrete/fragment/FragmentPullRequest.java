package br.com.desafioconcrete.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.List;

import br.com.desafioconcrete.PullRequestActivity;
import br.com.desafioconcrete.R;
import br.com.desafioconcrete.adapter.PullRequestAdapter;
import br.com.desafioconcrete.model.PullRequest;
import br.com.desafioconcrete.util.GsonParser;
import br.com.desafioconcrete.util.Util;
import br.com.desafioconcrete.volley.RequestHelper;

/**
 * Criado por Renato em 29/11/2016.
 */

public class FragmentPullRequest extends FragmentApp {

    private final String LOG_TAG = "FragmentPullRequest";
    private final String IS_SEARCHING = "IS_SEARCHING";
    private final String PREF_ERRO_CONEXAO = "erroConexao";

    private transient RecyclerView lv;
    private transient ProgressBar progress;
    private transient View erroConexao;
    private transient SwipeRefreshLayout swipeContainer;

    private boolean isSearching = false;
    private boolean isErroConexao = false;

    private List<PullRequest> pullRequests;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(IS_SEARCHING, isSearching);
        outState.putBoolean(PREF_ERRO_CONEXAO, isErroConexao);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pull_request, null);

        lv = (RecyclerView) v.findViewById(R.id.rvPullRequest);
        erroConexao = v.findViewById(R.id.llErroConexao);
        swipeContainer = (SwipeRefreshLayout) v.findViewById(R.id.swipeContainer);
        progress = (ProgressBar) v.findViewById(R.id.progress);

        if(savedInstanceState != null){
            isSearching = savedInstanceState.getBoolean(IS_SEARCHING);
            isErroConexao = savedInstanceState.getBoolean(PREF_ERRO_CONEXAO);
        }

        lv.setLayoutManager(new LinearLayoutManager(getActivity()));

        TextView tvTentarNovamente = (TextView) erroConexao.findViewById(R.id.tvTentarNovamente);
        tvTentarNovamente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataFromServer(true);
            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDataFromServer(false);
            }
        });
        swipeContainer.setColorSchemeResources(Util.getColorsSwipe());
        swipeContainer.setProgressViewOffset(false, 20, getResources().getDimensionPixelSize(R.dimen.margin_top_swipe_refresh));

        if(pullRequests != null){
            receivedData(Util.RESPONSE_OK, false);
            Log.d(LOG_TAG, "pull requests NOT NULL");
        }
        else if(isSearching){
            showProgressBar();
        }
        else if(!isErroConexao){
            getDataFromServer(true);
        }
        else {
            receivedData(Util.RESPONSE_ERROR, true);
        }

        return v;
    }

    private void receivedData(int status, boolean hideView) {
        if(status == Util.RESPONSE_OK) {
            setAdapter();
        }
        else {
            showError(hideView);
        }
    }

    private void showError(boolean hideView) {
        if(swipeContainer != null && swipeContainer.isRefreshing())
            swipeContainer.setRefreshing(false);

        if(!hideView) {
            Toast.makeText(getActivity(), "Erro ao atualizar", Toast.LENGTH_SHORT).show();
            return;
        }

        lv.setVisibility(View.GONE);
        erroConexao.setVisibility(View.VISIBLE);
        progress.setVisibility(View.GONE);
        isErroConexao = true;
    }

    private void setAdapter() {
        lv.setVisibility(View.VISIBLE);
        erroConexao.setVisibility(View.GONE);
        progress.setVisibility(View.GONE);
        isErroConexao = false;

        int totalOpened = 0;
        for(PullRequest pr : pullRequests) {
            if(pr.getState().equals("open"))
                totalOpened++;
        }
        ((PullRequestActivity) getActivity()).setPrOpen(totalOpened);
        ((PullRequestActivity) getActivity()).setPrClose(pullRequests.size() - totalOpened);

        if(lv.getAdapter() == null) {
            PullRequestAdapter adapter = new PullRequestAdapter(getActivity(), pullRequests);
            lv.setAdapter(adapter);
        }
        else {
            ((PullRequestAdapter) lv.getAdapter()).setPullRequests(pullRequests);
        }

        if(swipeContainer != null && swipeContainer.isRefreshing())
            swipeContainer.setRefreshing(false);
    }

    public void getDataFromServer(final boolean hideView) {
        if(isSearching || !isAdded())
            return;

        isSearching = true;

        if(hideView) {
            progress.setVisibility(View.VISIBLE);
            erroConexao.setVisibility(View.GONE);
            lv.setVisibility(View.GONE);
        }
        else {
            if (swipeContainer != null && !swipeContainer.isRefreshing()) {
                swipeContainer.post(new Runnable() {
                    @Override
                    public void run() {
                        if (isSearching)
                            swipeContainer.setRefreshing(true);
                    }
                });
            }
        }

        String ownerName = ((PullRequestActivity) getActivity()).getOwnerName();
        String repoName = ((PullRequestActivity) getActivity()).getRepoName();

        RequestHelper.getDataString(getString(R.string.pull_request_url, ownerName, repoName), new Response.Listener<String>() {
            @Override
            public void onResponse(String data) {
                isSearching = false;
                if (isDestroied() && !isAdded())
                    return;

                pullRequests = new GsonParser().toPullRequestList(data);

                if (isDadosVazios(pullRequests)) {
                    receivedData(Util.RESPONSE_OK, hideView);
                    Toast.makeText(getActivity(), "Nenhum dado obtido.", Toast.LENGTH_SHORT).show();
                }
                else {
                    receivedData(Util.RESPONSE_OK, hideView);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                isSearching = false;
                if (isDestroied())
                    return;

                receivedData(Util.RESPONSE_ERROR, hideView);
            }
        });
    }

    private boolean isDadosVazios(List<PullRequest> prs) {
        if(prs != null && prs.size() > 0)
            return false;

        return true;
    }

    private void showProgressBar() {
        progress.setVisibility(View.VISIBLE);
        erroConexao.setVisibility(View.GONE);
        lv.setVisibility(View.GONE);
    }
}
