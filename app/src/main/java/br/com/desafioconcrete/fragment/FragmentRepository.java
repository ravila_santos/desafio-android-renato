package br.com.desafioconcrete.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.Date;

import br.com.desafioconcrete.R;
import br.com.desafioconcrete.adapter.RepositoryAdapter;
import br.com.desafioconcrete.model.Repositories;
import br.com.desafioconcrete.util.CacheHelper;
import br.com.desafioconcrete.util.GsonParser;
import br.com.desafioconcrete.util.Util;
import br.com.desafioconcrete.volley.RequestHelper;

/**
 * Criado por Renato em 29/11/2016.
 */

public class FragmentRepository extends FragmentApp {

    private final String LOG_TAG = "FragmentRepository";
    private final String IS_SEARCHING = "IS_SEARCHING";
    private final String PREF_ERRO_CONEXAO = "erroConexao";
    private final String PREF_REACH_END = "PREF_REACH_END";
    private final String PREF_CURRENT_PAGE = "PREF_CURRENT_PAGE";
    private final String CACHE_KEY_JSON = "CACHE_KEY_JSON";
    private final Long CACHE_ID_JSON = 1L;
    private final Long MIN_30_IN_MILESEGUNDOS = 1800000L;// 30*60*1000;
    public static final String PREF_TIME_CACHE_JSON = "time_home_json";

    private transient RecyclerView lv;
    private transient ProgressBar progress;
    private transient View erroConexao;
    private transient SwipeRefreshLayout swipeContainer;

    private boolean isSearching = false;
    private boolean isErroConexao = false;
    private boolean isReachEnd = false;
    private int currentPage;

    private Repositories repositories;

    public boolean isReachEnd() {
        return isReachEnd;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(IS_SEARCHING, isSearching);
        outState.putBoolean(PREF_ERRO_CONEXAO, isErroConexao);
        outState.putBoolean(PREF_REACH_END, isReachEnd);
        outState.putInt(PREF_CURRENT_PAGE, currentPage);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_repository, null);

        lv = (RecyclerView) v.findViewById(R.id.rvHome);
        erroConexao = v.findViewById(R.id.llErroConexao);
        swipeContainer = (SwipeRefreshLayout) v.findViewById(R.id.swipeContainer);
        progress = (ProgressBar) v.findViewById(R.id.progress);

        if(savedInstanceState != null){
            isSearching = savedInstanceState.getBoolean(IS_SEARCHING);
            isErroConexao = savedInstanceState.getBoolean(PREF_ERRO_CONEXAO);
            isReachEnd = savedInstanceState.getBoolean(PREF_REACH_END);
            currentPage = savedInstanceState.getInt(PREF_CURRENT_PAGE);
        }
        else {
            currentPage = 1;
        }

        lv.setLayoutManager(new LinearLayoutManager(getActivity()));

        TextView tvTentarNovamente = (TextView) erroConexao.findViewById(R.id.tvTentarNovamente);
        tvTentarNovamente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataFromServerPageOne(true);
            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDataFromServerPageOne(false);
            }
        });
        swipeContainer.setColorSchemeResources(Util.getColorsSwipe());
        swipeContainer.setProgressViewOffset(false, 20, getResources().getDimensionPixelSize(R.dimen.margin_top_swipe_refresh));

        if(repositories != null) {
            receivedData(Util.RESPONSE_OK, false);
            Log.d(LOG_TAG, "repositories NOT NULL");
        }
        else if(isSearching) {
            showProgressBar();
        }
        else if(!isErroConexao) {
            String jsonCache = new CacheHelper(getActivity()).getSavedJson(CACHE_KEY_JSON, CACHE_ID_JSON);
            if(jsonCache != null && !jsonCache.equals("")) {
                repositories = new GsonParser().toRepositories(jsonCache);

                SharedPreferences prefs = getActivity().getSharedPreferences(Util.SHARED_PREFERENCES, Context.MODE_PRIVATE);
                Long lastUpdate = prefs.getLong(PREF_TIME_CACHE_JSON, 0L);
                Long timeNow = new Date().getTime();
                if (timeNow - lastUpdate >= MIN_30_IN_MILESEGUNDOS)
                    getDataFromServerPageOne(false);

                receivedData(Util.RESPONSE_OK, false);
            }
            else {
                getDataFromServerPageOne(true);
            }
        }
        else {
            receivedData(Util.RESPONSE_ERROR, true);
        }

        return v;
    }

    private void receivedData(int status, boolean hideView) {
        if(status == Util.RESPONSE_OK) {
            setAdapter();
        }
        else {
            showError(hideView);
        }
    }

    private void showError(boolean hideView) {
        if(swipeContainer != null && swipeContainer.isRefreshing())
            swipeContainer.setRefreshing(false);

        if(!hideView) {
            Toast.makeText(getActivity(), "Erro ao atualizar", Toast.LENGTH_SHORT).show();
            return;
        }

        lv.setVisibility(View.GONE);
        erroConexao.setVisibility(View.VISIBLE);
        progress.setVisibility(View.GONE);
        isErroConexao = true;
    }

    private void setAdapter() {
        lv.setVisibility(View.VISIBLE);
        erroConexao.setVisibility(View.GONE);
        progress.setVisibility(View.GONE);
        isErroConexao = false;

        if(lv.getAdapter() == null) {
            RepositoryAdapter adapter = new RepositoryAdapter(getActivity(), this, repositories.getItems());
            lv.setAdapter(adapter);
        }
        else {
            ((RepositoryAdapter) lv.getAdapter()).setRepositories(repositories.getItems());
        }

        if(swipeContainer != null && swipeContainer.isRefreshing())
            swipeContainer.setRefreshing(false);
    }

    public void getDataFromServerPageOne(final boolean hideView) {
        if(isSearching || !isAdded())
            return;

        isSearching = true;
        currentPage = 1;

        if(hideView) {
            progress.setVisibility(View.VISIBLE);
            erroConexao.setVisibility(View.GONE);
            lv.setVisibility(View.GONE);
        }
        else {
            if (swipeContainer != null && !swipeContainer.isRefreshing()) {
                swipeContainer.post(new Runnable() {
                    @Override
                    public void run() {
                        if (isSearching)
                            swipeContainer.setRefreshing(true);
                    }
                });
            }
        }

        RequestHelper.getDataString(getString(R.string.repositories_url, currentPage+""), new Response.Listener<String>() {
            @Override
            public void onResponse(String data) {
                isSearching = false;
                if (isDestroied() && !isAdded())
                    return;

                repositories = new GsonParser().toRepositories(data);

                if (isDadosVazios(repositories)) {
                    isReachEnd = true;
                    receivedData(Util.RESPONSE_OK, hideView);
                    Toast.makeText(getActivity(), "Nenhum dado obtido.", Toast.LENGTH_SHORT).show();
                }
                else {
                    isReachEnd = false;
                    receivedData(Util.RESPONSE_OK, hideView);
                    try {
                        new CacheHelper(getActivity()).saveJson(CACHE_KEY_JSON, CACHE_ID_JSON, data);
                        SharedPreferences prefs = getActivity().getSharedPreferences(Util.SHARED_PREFERENCES, Context.MODE_PRIVATE);
                        prefs.edit().putLong(PREF_TIME_CACHE_JSON, new Date().getTime()).apply();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                isSearching = false;
                if (isDestroied())
                    return;

                receivedData(Util.RESPONSE_ERROR, hideView);
            }
        });
    }

    public void loadMoreData() {
        if(isReachEnd || isSearching)
            return;

        isSearching = true;
        currentPage++;

        RequestHelper.getDataString(getString(R.string.repositories_url, currentPage+""), new Response.Listener<String>() {
            @Override
            public void onResponse(String data) {
                isSearching = false;
                if (isDestroied() && !isAdded())
                    return;

                Repositories repos = null;
                try {
                    repos = new GsonParser().toRepositories(data);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if(repos != null && repos.getItems() != null)
                    repositories.getItems().addAll(repos.getItems());

                if (isDadosVazios(repos)) {
                    isReachEnd = true;
                    ((RepositoryAdapter) lv.getAdapter()).setRepositories(repositories.getItems());
                    Toast.makeText(getActivity(), "Nenhum dado obtido na página " + currentPage, Toast.LENGTH_SHORT).show();
                }
                else {
                    isReachEnd = false;
                    ((RepositoryAdapter) lv.getAdapter()).setRepositories(repositories.getItems());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                isSearching = false;
                if (isDestroied())
                    return;

                isReachEnd = true;
                ((RepositoryAdapter) lv.getAdapter()).setRepositories(repositories.getItems());
                Toast.makeText(getActivity(), "Erro ao carregar página: " + currentPage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean isDadosVazios(Repositories reps) {
        if(reps != null && reps.getItems() != null && reps.getItems().size() > 0)
            return false;

        return true;
    }

    private void showProgressBar() {
        progress.setVisibility(View.VISIBLE);
        erroConexao.setVisibility(View.GONE);
        lv.setVisibility(View.GONE);
    }
}
