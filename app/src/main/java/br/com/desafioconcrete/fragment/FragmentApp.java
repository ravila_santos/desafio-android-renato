package br.com.desafioconcrete.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import java.io.Serializable;

/**
 * Criado por Renato em 29/11/2016.
 */

public abstract class FragmentApp extends Fragment implements Serializable {

    private static final long serialVersionUID = 1L;
    private boolean isDestroied = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        isDestroied = false;
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        isDestroied = true;
        super.onDestroy();
    }

    public boolean isDestroied() {
        return isDestroied;
    }
}