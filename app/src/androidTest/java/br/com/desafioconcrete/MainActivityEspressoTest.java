package br.com.desafioconcrete;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collection;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.runner.lifecycle.Stage.RESUMED;
import static org.junit.Assert.assertTrue;

/**
 * Criado por Renato em 01/12/2016.
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityEspressoTest {

    @Rule
    public ActivityTestRule<MainActivity> rule = new ActivityTestRule<>(MainActivity.class);

    private static final int ITEMS_FROM_API = 30;
    private static final int ITEM_PROGRESS = 1;

    @Test
    public void checkGoToPRActivity() {
        onView(withId(R.id.rvHome)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        Activity activity = getActivityInstance();
        boolean b = (activity instanceof PullRequestActivity);
        assertTrue(b);
    }

    @Test
    public void checkRecyclerViewSize() {
        onView(withId(R.id.rvHome)).check(new RecyclerViewItemCountAssertion(ITEMS_FROM_API+ITEM_PROGRESS));
    }

    @Test
    public void checkLoadMoreDataAndRotateScreen() {
        onView(withId(R.id.rvHome)).perform(RecyclerViewActions.scrollToPosition(ITEMS_FROM_API));
        //wait until load new data
        onView(withId(R.id.rvHome)).check(new RecyclerViewItemCountAssertion(ITEMS_FROM_API+ITEMS_FROM_API+ITEM_PROGRESS));

        rotateScreen();

        onView(withId(R.id.rvHome)).check(new RecyclerViewItemCountAssertion(ITEMS_FROM_API+ITEMS_FROM_API+ITEM_PROGRESS));
    }

    @Test
    public void onlyRotateScreen() {
        rotateScreen();
        onView(withId(R.id.rvHome)).check(new RecyclerViewItemCountAssertion(ITEMS_FROM_API+ITEM_PROGRESS));
    }

    private void rotateScreen() {
        Context context = InstrumentationRegistry.getTargetContext();
        int orientation = context.getResources().getConfiguration().orientation;

        Activity activity = rule.getActivity();
        activity.setRequestedOrientation(
                (orientation == Configuration.ORIENTATION_PORTRAIT) ?
                        ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public Activity getActivityInstance() {
        final Activity[] activity = new Activity[1];
        InstrumentationRegistry.getInstrumentation().runOnMainSync(new Runnable( ) {
            public void run() {
                Activity currentActivity = null;
                Collection resumedActivities = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(RESUMED);
                if (resumedActivities.iterator().hasNext()){
                    currentActivity = (Activity) resumedActivities.iterator().next();
                    activity[0] = currentActivity;
                }
            }
        });

        return activity[0];
    }
}
